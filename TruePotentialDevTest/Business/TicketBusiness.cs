﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruePotentialDevTest.Data;
using TruePotentialDevTest.Models;
using System.Data.Entity;

namespace TruePotentialDevTest.Business
{
    public class TicketBusiness : BusinessBase
    {
        /// <summary>
        /// Creates a new ticket in the database
        /// </summary>
        /// <param name="ticket">Ticket object</param>
        /// <param name="creator">User object</param>
        public void CreateTicket(NewTicketViewModel ticket, int creatorId)
        {
            using (TPContext context = new TPContext())
            {
                Ticket ticketToAdd = new Ticket();
                ticketToAdd.Name = ticket.Name;
                ticketToAdd.Creator = context.Users.FirstOrDefault(x => x.Id == creatorId);
                ticketToAdd.CreationDate = DateTime.Now;
                ticketToAdd.Content = ticket.Content;

                context.Tickets.Add(ticketToAdd);

                context.SaveChanges();
            }
        }

        /// <summary>
        /// Gets a ticket from the database with a given Id
        /// </summary>
        /// <param name="id">Ticket id</param>
        /// <returns>Ticket</returns>
        public Ticket GetTicket(int id)
        {
            Ticket toReturn;
            using (TPContext context = new TPContext())
            {
                //context.Tickets.Load();
                //context.Responses.Load();
                toReturn = context.Tickets
                    .Include(x => x.Creator)
                    .Include(x => x.Holder)
                    .Include(x => x.Responses.Select(y => y.Creator))
                    .FirstOrDefault(x => x.Id == id);
            }

            return toReturn;
        }

        /// <summary>
        /// Gets a list of tickets from the database
        /// </summary>
        /// <returns>Ticket list</returns>
        public List<Ticket> GetTickets()
        {
            List<Ticket> tickets;
            using (TPContext context = new TPContext())
            {
                //context.Configuration.LazyLoadingEnabled = false;
                tickets = context.Tickets
                    .Include(c => c.Creator)
                    .Include(c => c.Holder)
                    .ToList();

                return tickets;
            }
        }

        /// <summary>
        /// Gets ticket for a specific user
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns></returns>
        public List<Ticket> GetTickets(int userId)
        {
            List<Ticket> tickets;
            using (TPContext context = new TPContext())
            {
                //context.Configuration.LazyLoadingEnabled = false;
                tickets = context.Tickets
                    .Include(c => c.Creator)
                    .Include(c => c.Holder)
                    .Where(c => c.Creator.Id == userId)
                    .ToList();

                return tickets;
            }
        }

        /// <summary>
        /// Gets a list of tickets from the database
        /// and converts them into ListTickets
        /// </summary>
        /// <returns></returns>
        public List<ListTicketViewModel> GetTicketsForList()
        {
            List<ListTicketViewModel> listTickets = new List<ListTicketViewModel>();
            List<Ticket> tickets = GetTickets();

            foreach (Ticket ticket in tickets)
            {
                ListTicketViewModel listTicket = new ListTicketViewModel();
                listTicket.Id = ticket.Id;
                listTicket.Name = ticket.Name;
                listTicket.Status = ticket.Status;
                listTicket.CreationDate = ticket.CreationDate;
                listTicket.CreatorName = ticket.Creator.FullName;

                if (ticket.Holder != null)
                    listTicket.HolderName = ticket.Holder.FullName;
                else listTicket.HolderName = "(none)";

                listTickets.Add(listTicket);
            }

            return listTickets;
        }

        /// <summary>
        /// Gets tickets with a specific IDand turns them into a list
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>List of tickets</returns>
        public List<ListTicketViewModel> GetTicketsForList(int userId)
        {
            List<ListTicketViewModel> listTickets = new List<ListTicketViewModel>();
            List<Ticket> tickets = GetTickets(userId);

            foreach (Ticket ticket in tickets)
            {
                ListTicketViewModel listTicket = new ListTicketViewModel();
                listTicket.Id = ticket.Id;
                listTicket.Name = ticket.Name;
                listTicket.Status = ticket.Status;
                listTicket.CreationDate = ticket.CreationDate;
                listTicket.CreatorName = ticket.Creator.FullName;

                if (ticket.Holder != null)
                    listTicket.HolderName = ticket.Holder.FullName;
                else listTicket.HolderName = "(none)";

                listTickets.Add(listTicket);
            }

            return listTickets;
        }

        /// <summary>
        /// Changes a ticket's status to a given status
        /// </summary>
        /// <param name="ticketId">Ticket ID</param>
        /// <param name="status">Status</param>
        public void SetTicketStatus(int ticketId, Ticket.TicketStatus status)
        {
            // Open the context
            using (TPContext context = new TPContext())
            {
                // Get ticket from the database
                Ticket ticket = context.Tickets.FirstOrDefault(x => x.Id == ticketId);
                // Set its status to the provided status
                ticket.Status = status;
                // Update the UpdateDate
                ticket.UpdateDate = DateTime.Now;
                // Attach the ticket to the context
                context.Tickets.Attach(ticket);
                
                // Tell EF it's been modified
                context.Entry(ticket).State = EntityState.Modified;

                // Save changes to the database
                context.SaveChanges();
            }
        }
    }
}