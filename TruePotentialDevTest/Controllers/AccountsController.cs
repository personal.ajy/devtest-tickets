﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using TruePotentialDevTest.Business;
using TruePotentialDevTest.Models;
using TruePotentialDevTest.Helpers;

namespace TruePotentialDevTest.Controllers
{
    public class AccountsController : Controller
    {
        // GET: Accounts
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Login login)
        {
            bool userExists;
            User user;

            using (UserBusiness business = new UserBusiness())
            {
                // Check if the user exists
                userExists = business.IsUser(login.Email);

                // Only fetch the user's email if they exist
                if (userExists)
                    user = business.GetUser(login.Email);
                else user = null;
            }

            if (!userExists)
            {
                // If the user doesn't exist, send them to an error page
                return View("~/Views/Error/Index.cshtml", new ErrorViewModel { Message = String.Format("It looks like your email '{0}' isn't registered under an account!", login.Email) });
            }
            else if (Encryption.MD5Hash(login.Password) == user.Password)
            {
                // Set up session properties
                Session["LoggedIn"] = true;
                Session["UserId"] = user.Id;
                Session["UserFullName"] = user.FullName;
                Session["UserEmail"] = user.Email;
                Session["IsAdmin"] = (user.UserType == Models.User.Type.Admin);

                if (user.UserType == Models.User.Type.Admin)
                {
                    // If they're an admin, redirect them to the admin index page
                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    // If they're a regular user, redirect them to the CreateTicket page
                    return RedirectToAction("CreateTicket", "Home");
                }
            }
            else
            {
                // Send user to error page if the password is incorrect
                return View("~/Views/Error/Index.cshtml", new ErrorViewModel { Message = "The password you have entered is incorrect!" });
            }
        }

        public ActionResult Logout()
        {
            // Redirect to error page if the user isn't logged in
            if (!Helpers.Sessions.IsUserLoggedIn(Session))
                return View("~/Views/Error/Index.cshtml", new ErrorViewModel { Message = "You are not logged in!" });

            // Clear the session
            Session.Clear();

            // Send user to the login page
            return RedirectToAction("Login", "Accounts");
        }
    }
}