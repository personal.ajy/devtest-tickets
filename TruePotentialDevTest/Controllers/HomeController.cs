﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TruePotentialDevTest.Business;
using TruePotentialDevTest.Data;
using TruePotentialDevTest.Models;

namespace TruePotentialDevTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateTicket()
        {
            // Only logged-in users can access this page
            if (Helpers.Sessions.IsUserLoggedIn(Session) == false)
                return RedirectToAction("Login", "Accounts");

            return View();
        }

        [HttpPost]
        public ActionResult CreateTicket(NewTicketViewModel ticket)
        {
            // Only logged-in users can access this page
            if (Helpers.Sessions.IsUserLoggedIn(Session) == false)
                return RedirectToAction("Login", "Accounts");

            if (String.IsNullOrEmpty(ticket.Name) || String.IsNullOrEmpty(ticket.Content))
            {
                return View("~/Views/Error/Index.cshtml", new ErrorViewModel { Message = "Please provide both a name and some content!" });
            }
            else
            {
                using (TicketBusiness business = new Business.TicketBusiness())
                {
                    business.CreateTicket(ticket, (int)Session["UserId"]);
                }

                return RedirectToAction("Lobby", "Home");
            }
        }

        public ActionResult Lobby()
        {
            if (Helpers.Sessions.IsUserLoggedIn(Session) == false)
                return RedirectToAction("Login", "Accounts");

            // Create a new view model
            MyTicketViewModel myTicketPage = new MyTicketViewModel();
            using (TicketBusiness business = new TicketBusiness())
            {
                // Grab tickets from the database and store in the view model
                myTicketPage.Tickets = business.GetTicketsForList((int)Session["UserId"]);
            }

            return View(myTicketPage);
        }
        
        public ActionResult ViewTicket()
        {
            // Only logged in users can see this page
            if (Helpers.Sessions.IsUserLoggedIn(Session) == false)
                return RedirectToAction("Login", "Accounts");

            //Create a new view model instance
            ViewTicketViewModel model = new ViewTicketViewModel();

            // Grab the ticket ID from routedata
            int ticketId = Int32.Parse((string)RouteData.Values["id"]);

            // Get ticket data from the database with the ID
            using (TicketBusiness ticketBusiness = new TicketBusiness())
            {
                model.Ticket = ticketBusiness.GetTicket(ticketId);
            }

            // Handle null Holder value
            if (model.Ticket.Holder == null)
            {
                model.Ticket.Holder = new User();
                model.Ticket.Holder.FullName = "(none)";
            }

            // Return view model
            return View(model);
        }
    }
}