﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TruePotentialDevTest.Business;
using TruePotentialDevTest.Models;

namespace TruePotentialDevTest.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            // Only admins can access this page
            if (Helpers.Sessions.IsUserAdmin(Session) == false)
                return RedirectToAction("Login", "Accounts");

            // Create a new view model
            AdminViewModel adminPage = new AdminViewModel();
            using (TicketBusiness business = new TicketBusiness())
            {
                // Grab tickets from the database and store in the view model
                adminPage.Tickets = business.GetTicketsForList();
            }

            // Show the view
            return View(adminPage);
        }

        public ActionResult ViewTicket()
        {
            // Only admins can access this page
            if (Helpers.Sessions.IsUserAdmin(Session) == false)
                return RedirectToAction("Login", "Accounts");

            // Get ticket from route value
            int ticketId = Int32.Parse(RouteData.Values["id"].ToString());

            // Get ticket object from database
            Ticket ticket;
            using (TicketBusiness business = new TicketBusiness())
            {
                ticket = business.GetTicket(ticketId);
            }

            // Show the ticket holder as empty if it isn't being held
            if (ticket.Holder == null)
            {
                ticket.Holder = new User();
                ticket.Holder.FullName = "(none)";
            }

            // Create a new instance of view model
            ViewTicketViewModel viewModel = new ViewTicketViewModel();
            // Store tickets in view model
            viewModel.Ticket = ticket;
            
            // Return view
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult PostResponse(ViewTicketViewModel viewModel)
        {
            // Only admins can post responses
            if (Helpers.Sessions.IsUserAdmin(Session) == false)
                return RedirectToAction("Login", "Accounts");

            // Redirect to error page if response has not been given
            if (viewModel.ResponseContent.Trim() == "")
                return View("~/Views/Error/Index.cshtml", new ErrorViewModel { Message = "Please provide some text! " });

            // Add the response
            using (ResponseBusiness business = new ResponseBusiness())
            {
                business.AddResponse(viewModel.Ticket.Id, (int)Session["UserId"], viewModel.ResponseContent.Trim());
            }

            // Refresh the page
            return RedirectToAction("ViewTicket", new { id = viewModel.Ticket.Id });
        }

        [HttpPost]
        public ActionResult CloseTicket(ViewTicketViewModel viewModel)
        {
            // Only admins can close tickets
            if (Helpers.Sessions.IsUserAdmin(Session) == false)
                return RedirectToAction("Login", "Accounts");

            using (TicketBusiness ticketBusiness = new TicketBusiness())
            {
                // Set the ticket's status to closed
                ticketBusiness.SetTicketStatus(viewModel.Ticket.Id, Ticket.TicketStatus.Closed);
            }

            // Refresh the page
            return RedirectToAction("ViewTicket", new { id = viewModel.Ticket.Id });
        }

        [HttpPost]
        public ActionResult OpenTicket(ViewTicketViewModel viewModel)
        {
            // Only admins can open and claim tickets
            if (Helpers.Sessions.IsUserAdmin(Session) == false)
                return RedirectToAction("Login", "Accounts");

            using (TicketBusiness ticketBusiness = new TicketBusiness())
            {
                // Set the ticket's status to open
                ticketBusiness.SetTicketStatus(viewModel.Ticket.Id, Ticket.TicketStatus.Claimed);
            }

            // Refresh the page
            return RedirectToAction("ViewTicket", new { id = viewModel.Ticket.Id });
        }

        [HttpPost]
        public ActionResult ResolveTicket(ViewTicketViewModel viewModel)
        {
            // Only admins can resolve tickets
            if (Helpers.Sessions.IsUserAdmin(Session) == false)
                return RedirectToAction("Login", "Accounts");

            using (TicketBusiness ticketBusiness = new TicketBusiness())
            {
                // Set the ticket's status to claimed
                ticketBusiness.SetTicketStatus(viewModel.Ticket.Id, Ticket.TicketStatus.Resolved);
            }

            // Refresh the page
            return RedirectToAction("ViewTicket", new { id = viewModel.Ticket.Id });
        }
    }
}