﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Models
{
    public class NewTicketViewModel
    {
        public string Name { get; set; }

        public string Content { get; set; }

        public User Owner { get; set; }
    }
}