﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Models
{
    public class User
    {
        public enum Type
        {
            Client,
            Admin
        }

        [Key]
        public int Id { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public string Password { get; set; }

        public User.Type UserType { get; set; }

        public bool IsDeleted { get; set; }
    }
}