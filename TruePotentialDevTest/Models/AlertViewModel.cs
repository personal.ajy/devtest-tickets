﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Models
{
    public class AlertViewModel
    {
        public string Title { get; set; }

        public string Message { get; set; }
    }
}