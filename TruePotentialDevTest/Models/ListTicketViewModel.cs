﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Models
{
    public class ListTicketViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreationDate { get; set; }

        public string CreatorName { get; set; }

        public string HolderName { get; set; }

        public Ticket.TicketStatus Status { get; set; }
    }
}