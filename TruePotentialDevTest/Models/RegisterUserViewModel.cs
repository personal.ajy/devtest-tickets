﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Models
{
    public class RegisterUserViewModel
    {
        public string Email { get; set; }

        public string FullName { get; set; }

        public string Password { get; set; }

        public string PasswordRepeated { get; set; }
    }
}