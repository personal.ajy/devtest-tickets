﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruePotentialDevTest.Helpers
{
    public static class Sessions
    {
        /// <summary>
        /// Checks if a user is logged in
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static bool IsUserLoggedIn(HttpSessionStateBase session)
        {
            if (session["LoggedIn"] == null)
                return false;

            return (bool)session["LoggedIn"];
        }

        /// <summary>
        /// Checks if a user is an admin
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static bool IsUserAdmin(HttpSessionStateBase session)
        {
            if (!Helpers.Sessions.IsUserLoggedIn(session))
                return false;

            return (bool)session["IsAdmin"];
        }
    }
}