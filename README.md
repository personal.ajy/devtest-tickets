# Development Test - Ticket System #
This was a development test that required a simple ticket system be created.

### Features ###

* Users can log in as either clients or admins
* Clients can create new tickets
* Admins can view all submitted tickets
* Admins can change a ticket's status (mark as closed, resolved or claimed) and post replies to tickets

### More Information ###
The web application is written in .NET's C# using the ASP.NET MVC framework. All data is stored in an SQL Server database and Entity Framework is used to handle communication between the database and the site. The UI is a mix of my own design and (mainly) bootstrap components.

The code for this is admittedly slightly rushed due to time constraints and a particularly bad week...